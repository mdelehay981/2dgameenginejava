package com.mygdx.game;	// comment this line out for non gradle version  

import com.badlogic.gdx.math.Vector2;

public class ItemDef {
    public Vector2 position;
    public Class<?> type;

    public ItemDef(Vector2 position, Class<?> type) {
        this.position = position;
        this.type = type;
    }
}
